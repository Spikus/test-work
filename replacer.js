    /**
     * Utils
     *
     * @description url image loader
     * @version     0.9.2
     * @author      Daniel Egorov
     * @company     a@livarava.com
     * @requires    none
     * link https://www.livarava.com
     */

(function () {
    // замкнем переменные только для скрипта
    var _input = {},                                        // Обьект под поле Ввода
        _data = {},                                         // Информация из запроса API гугла
        _videoid = '0',                                     // ID для видео
        _error = '',                                        // Ошибка + RegExp для ссылки на Youtube
        _youPattern = new RegExp('^(https?\:\/\/)?((www\.)?youtube\.com|youtu\.?be)\/.+$');

    init = function () {
        // Инициализация                  
        // Присваивает ссылку на обьект и добовляте колбек на событие изменения поля
        _input = document.getElementById('inputUrl');
        _input.addEventListener('change', urlValidate);
    },
          urlValidate = function () {
              // Валидации введеных данных
              // Добовлет анимацию обработки запроса и проверяет на корректоность ссылка c YouTube
              _input.className = _input.className + ' onload';
              if (!_youPattern.test(_input.value)) {errorChange('Похоже что эта ссылка не соответсвует видео на YouTube');return ;}
              getVideoId();
          },
          getVideoId = function () {
              // Парсинг VideoID
              // Youtube передает ссылку в 2х вариантах
              if (_input.value.indexOf('//youtu.be/') > 0) {
              _videoid = _input.value.split('//youtu.be/')[1].split('?t')[0];} else if (_input.value.indexOf('watch') > 0)
              { _videoid = _input.value.split('watch?v=')[1].split('&')[0]; } else
              {errorChange('В URL включено слишком много параметров мы не смогли распознать');return ;}
              loadImage();
          },
          loadImage = function () {
              // Загрузка данных из YouTube Api
              // AIzaSyCAaorLKDiqHnjUt9cbUuL8CMD4zFP3LvY - ключ
              var xhr = new XMLHttpRequest();
              var url = 'https://www.googleapis.com/youtube/v3/videos?id=' + _videoid + '&key=AIzaSyCAaorLKDiqHnjUt9cbUuL8CMD4zFP3LvY&part=snippet';
              xhr.open('GET', url, true);
              xhr.onreadystatechange = function () {
                  if (xhr.readyState != 4) return;
                  if (xhr.status != 200) {errorChange('Похоже какие-то проблемы с сервером YouTube');return ;}
                  if (xhr.status == 200) {_data = JSON.parse(xhr.responseText);dataRender();}
              };
              xhr.send();
          },
          dataRender = function (err) {
              // Вывод данных
              if (_data.items.length < 1)
              {errorChange('Ошибка! Нет таких видео на YouTube');return ;} else
              {errorChange('');}
              videoData = _data.items[0].snippet;
              document.getElementById('imgYoutube').src = videoData.thumbnails.high.url;
              document.getElementById('headYoutube').innerHTML = videoData.title;
          },
          errorChange = function (err) {
              // Вывод ошибок
              document.getElementById('errorLoad').innerHTML = err;
              _input.className = _input.className.split(' ')[0];
          };
    // Добавим инициализацию на кол-бек загрузки данных
    document.addEventListener('DOMContentLoaded', init());
})();
